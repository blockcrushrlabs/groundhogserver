#!/bin/bash
function up {
    echo "deploying api"
    docker-compose up -d
}

function init {
    docker build -t payapi:latest .
    docker run -it --rm -v postgres-data:/var/lib/postgresql/data -v $(pwd):/scripts --network=groundhogserver_paymentApi postgres:10-alpine psql -h db -U postgres -f "/scripts/create_dbs.sql"
    docker run -it --rm -v $(pwd):/home/node/app --network=groundhogserver_paymentApi payapi:latest sequelize db:migrate
}
"$@"