const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
const passport = require('passport');
const Strategy = require('passport-http-bearer').Strategy;
const models = require('models/index');

const app = express();



app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
const sequelize = new Sequelize('hackathon', 'postgres', 'password', {
    //host: 'localhost',
    host: 'db',
    dialect: 'postgres',
    pool: {
        max: 9,
        min: 0,
        idle: 10000
    }
});

sequelize.authenticate().then(() => {
    console.log("Success!");
}).catch((err) => {
    console.log(err);
});
app.use(passport.initialize());
models.sequelize.sync().then(function(){
    console.log("SYNCED");
});

app.use(function(err, req, res, next){
    if(err instanceof SyntaxError && err.status === 400 && 'body' in err){
        return res.status(400).json(
            {
                message: "Error: There was an issue with the provided parameters, please ensure JSON is error free"
            }
        )
    }
});

require('./server/routes')(app);
app.get('*', (req, res) => res.status(200).send({
    message: 'SWAG!!!!!!',
}));

module.exports = app;