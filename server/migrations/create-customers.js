module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('Customers', {
            id: {
                primaryKey: true,
                allowNull: true,
                type: Sequelize.INTEGER,
                autoIncrement: true,
                unique: true
            },
            firstName: {
                allowNull: false,
                type: Sequelize.STRING,
            },
            lastName: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            email: {
                allowNull: false,
                type: Sequelize.STRING,
                unique: true,
            },
            wallets: {
                allowNull: true,
                type: Sequelize.STRING
            },
            requiredConfirms: {
                allowNull: true,
                type: Sequelize.INTEGER
            },
            signatures: {
                allowNull: true,
                type: Sequelize.STRING
            },
            apiKey:{
                allowNull: true,
                type: Sequelize.UUID
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('Customers')
};