//look here tomorrow

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Payments', {
            merchantId: {
                type: Sequelize.INTEGER,
                onDelete: 'CASCADE',
                references: {
                    model: "Merchants",
                    key: "id",
                    as: "merchantid"
                }
            },
            customerId: {
                type: Sequelize.INTEGER,
                references: {
                    model: "Customers",
                    key: "id",
                    as: "customerid",
                }
            },
            merchantName:{
                type: Sequelize.STRING,
                allowNull: false
            },
            uuid: {
                type: Sequelize.UUID,
                allowNull: false,
                primaryKey: true,
            },
            transId: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            value: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            type: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            frequency: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            imageRef: {
                type: Sequelize.STRING,
                allowNull: false
            },
            requiredConfirms: {
                type: Sequelize.STRING,
                allowNullL: true,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('Payments')
};

