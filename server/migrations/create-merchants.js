module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('Merchants', {
            id:{
                allowNull: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement: true,
                unique: true
            },
            name: {
                allowNull: false,
                type: Sequelize.STRING,
                unique: true
            },
            email:{
              allowNull: false,
              unique: true,
              type: Sequelize.STRING
            },
            ethAddress: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            imageRef: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            description: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            apiKey:{
                allowNull: false,
                type: Sequelize.UUID
            }
        });
    },
    down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('Merchants')
};