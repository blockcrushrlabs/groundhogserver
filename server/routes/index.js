const merchantsControllers = require('../controllers').merchants;
const customersControllers = require('../controllers').customers;
const paymentsConntrollers = require('../controllers').payments;
const SQSControllers = require('../controllers').SQS;
const passport = require('passport');
const Strategy = require('passport-http-bearer').Strategy;
const Merchant = require('../models/index').Merchants;
const Customer = require('../models/index').Customers;

passport.use('MerchantStrat', new Strategy(
    function(token, done){
        Merchant.findOne({
            where:{
                apiKey: token
            }
        }).then(function(merchant){
            if(!merchant){
                return done(null, false)
            }
            return done(null, merchant);
        }).catch(function(err){
            return done("error with token format");
        })
    }
));

passport.use('CustomerStrat', new Strategy(
    function(token, done){
        console.log(token);
        Customer.findOne({
            where:{
                apiKey: token
            }
        }).then(function(customer){
            if(!customer){
                return done(null, false)
            }
            return done(null, customer);
        }).catch(function(err){
            return done("error with token format");
        })
    }
));

module.exports = (app) => {
    app.get('/welcome', (req, res) => res.status(200).json({
        message: "welcome to groundhog server"
    }));

    //merchant routes
    app.post('/merchants', merchantsControllers.register);
    app.put('/merchants', passport.authenticate('MerchantStrat', {session: false, failureRedirect: '/UNAUTHORIZED' }), merchantsControllers.update);
    app.delete('/merchants', passport.authenticate('MerchantStrat', {session: false, failureRedirect: '/UNAUTHORIZED' }), merchantsControllers.delete);
    app.get('/merchants', passport.authenticate('MerchantStrat', {session: false, failureRedirect: '/UNAUTHORIZED' }), paymentsConntrollers.merchantReport);

    //customer routes
    app.post('/customers', customersControllers.register);
    app.put('/customers', passport.authenticate('CustomerStrat', {session: false, failureRedirect: '/UNAUTHORIZED' }), customersControllers.update);
    app.delete('/customers', passport.authenticate('CustomerStrat', {session: false, failureRedirect: '/UNAUTHORIZED' }), customersControllers.delete);
    app.get('/customers', passport.authenticate('CustomerStrat', {session: false, failureRedirect: '/UNAUTHORIZED'}), paymentsConntrollers.customerReport)

    //payment routes
    app.post('/processToken', passport.authenticate('MerchantStrat', {session: false, failureRedirect: '/UNAUTHORIZED' }), paymentsConntrollers.processPaymentToken);
    app.post('/widgetPay', passport.authenticate('MerchantStrat', {session: false, failureRedirect: '/UNAUTHORIZED' }), paymentsConntrollers.widgetPayment);

    //testing and development routes
    app.post('/genSig', customersControllers.genSig);
    app.post('/QCREATE', SQSControllers.createQ);
    app.post('/GETQURL', SQSControllers.getQURL);
    app.post('/DELQ', SQSControllers.deleteQ);
    app.post('/QMESSAGE', SQSControllers.postMessage);
    app.post('/payments', paymentsConntrollers.create);

    //default redirect for unauthorized
    app.get('/UNAUTHORIZED', (req, res) => res.status(401).json({
        message: "YOU ARE NOT AUTHORIZED TO ACCESS THIS ENDPOINT"
    }))
};