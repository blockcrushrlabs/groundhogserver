'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Customers',[{
        firstName: "TEST1",
        lastName: "PERSON",
        email: "test1@test.com",
        signatures: "0x7BDD5C089da61e46106F6F21AafDE5d541695dFe",
        updatedAt: new Date,
        createdAt: new Date,
        requiredConfirms: 1
    },
    {
        firstName: "TEST2",
        lastName: "PERSON",
        email: "test2@test.com",
        signatures: "0x7BDD5C089da61e46106F6F21AafDE5d541695dFd",
        updatedAt: new Date(),
        createdAt: new Date(),
        requiredConfirms: 1
    },
        {
            firstName: "TEST3",
            lastName: "PERSON",
            email: "tes32@test.com",
            signatures: "0x7BDD5C089da61e46106F6F21AafDE5d541695dFc",
            updatedAt: new Date(),
            createdAt: new Date(),
            requiredConfirms: 1
        }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Customers', null, {});
  }
};
