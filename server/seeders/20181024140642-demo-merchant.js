'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Merchants',[{
          name: "TEST MERCHANT 1",
          email: "testMerch1@test.com",
          description: "TEST MERCHANT 1",
          imageRef: "RANDOM STRING",
          updatedAt: new Date,
          createdAt: new Date,
          apiKey: "63aa8dd8-ac7f-4b54-8fad-7b7c983f971c"
      },
          {
              name: "TEST MERCHANT 2",
              email: "testMerch2@test.com",
              description: "TEST MERCHANT 2",
              imageRef: "RANDOM STRING",
              updatedAt: new Date,
              createdAt: new Date,
              apiKey: "6f00eaca-3efd-4f02-a5f4-3480039aee1e"
          },
          {
              name: "TEST MERCHANT 3",
              email: "testMerch3@test.com",
              description: "TEST MERCHANT 3",
              imageRef: "RANDOM STRING",
              updatedAt: new Date,
              createdAt: new Date,
              apiKey: "dff90b0f-2b61-44eb-88bd-f038d11257ed"
          }], {});
},

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Merchants', null, {});
  }
};
