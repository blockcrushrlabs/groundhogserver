'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Payments',[{
        merchantId: 1,
        customerId: 1,
        merchantName: "TEST MERCHANT 1",
        uuid: "9bd3e857-1bdc-406e-ac42-7cd75757a03f",
        value: "100",
        type: "ETH",
        updatedAt: new Date,
        createdAt: new Date,
        imageRef: "TEST"
        },
        {
        merchantId: 1,
          customerId: 2,
          merchantName: "TEST MERCHANT 1",
            uuid: "3aad7919-6c6b-4d14-ac6e-e628669380ad",
          value: "100",
          type: "ETH",
          updatedAt: new Date,
          createdAt: new Date,
            imageRef: "TEST"
      },
        {
          merchantId: 1,
          customerId: 3,
          merchantName: "TEST MERCHANT 1",
            uuid: "35ac2109-7c93-451d-916b-16cb5d3dfd7a",
          value: "100",
          type: "ETH",
          updatedAt: new Date,
          createdAt: new Date,
            imageRef: "TEST"
        },
        {
    merchantId: 2,
    customerId: 1,
    merchantName: "TEST MERCHANT 2",
            uuid: "02f4fb53-d76f-4afc-bae4-bf6292879117",
    value: "100",
    type: "ETH",
    updatedAt: new Date,
    createdAt: new Date,
            imageRef: "TEST"
  }, {
          merchantId: 3,
          customerId: 1,
          merchantName: "TEST MERCHANT 2",
            uuid: "38ba5229-e22f-4dae-922e-6203d21c7ec5",
          value: "100",
          type: "ETH",
          updatedAt: new Date,
          createdAt: new Date,
            imageRef: "TEST"},
        {
            merchantId: 3,
            customerId: 1,
            merchantName: "TEST MERCHANT 2",
            uuid: "f06d249a-d5ce-4039-a046-8ef5075832f6",
            value: "100",
            type: "ETH",
            updatedAt: new Date,
            createdAt: new Date,
            imageRef: "TEST"}, ], {});
},

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
