'use strict'

//IF THIS DOESNT WORK YOU MIGHT HAVE TO MANUALLY CREATE THE CREATED AND UPDATED COLUMNS
module.exports = (sequelize, DataTypes) => {
    const Merchants = sequelize.define('Merchants', {
        id: {
          type: DataTypes.INTEGER,
          allowNull: true,
          primaryKey: true,
          autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        ethAddress: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        imageRef: {
            type: DataTypes.STRING,
            allowNull: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true
        },
        apiKey: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            allowNull: false
        }

    },

        {
            classMethods:{
                associate:function(models){
                    Merchants.hasMany(models.Payments, {foreignKey: 'merchantId'})
                }
            }
        }
    );
    /*
    Merchants.associate = (models) => {
        Merchants.hasMany(models.Payments, {
            foreignKey: 'merchantid',
            as: "payments"

        });
    };*/
    return Merchants;

};
