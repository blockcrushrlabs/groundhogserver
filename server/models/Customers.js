'use strict'

module.exports = (sequelize, DataTypes) => {
    const Customers = sequelize.define('Customers', {
        id: {
            type: DataTypes.INTEGER,
            allowNull: true,
            primaryKey: true,
            autoIncrement: true
        },
        firstName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        // spoof this for now, no need for table for hackathon, just read comma delimited tring
        wallets: {
            type: DataTypes.STRING,
            allowNull: true

        },
        requiredConfirms: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        signatures: {
            type: DataTypes.STRING,
            allowNull: true
        },
        apiKey: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            allowNull: false
        }
    },
        {
            classMethods:{
                associate:function(models){
                    Customers.hasMany(models.Payments, {foreignKey: 'customerId'})
                }
            }
        }
    );
    return Customers;

};