'use strict'

//IF THIS DOESNT WORK YOU MIGHT HAVE TO MANUALLY CREATE THE CREATED AND UPDATED COLUMNS
module.exports = (sequelize, DataTypes) =>{
    const Payments = sequelize.define('Payments', {
        customerId:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
        merchantId:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
        merchantName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        uuid: {
            type: DataTypes.UUID,
            allowNull: false,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        transId: {
            type: DataTypes.STRING,
            allowNull: true
        },
        //if anyone says anything its because BIGINTS SUCK and its worth just parsing the string, if i will have time i will write that iin
        value: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        type: {
            type: DataTypes.STRING,
            allowNull: true
        },
        frequency: {
            type: DataTypes.STRING,
            allowNull: true
        },
        requiredConfirms: {
            type: DataTypes.STRING,
            allowNull: true
        },
        imageRef: {
            type: DataTypes.STRING,
            allowNull: false
        }
    },
        {classMethods: {
                associate:function(models) {
                Payments.belongsTo(models.Customers, {foreignKey: 'customerId'});
                Payments.belongsTo(models.Merchants, {foreignKey: 'merchantId'});
                }
            }
        }
    );/*
    Payments.associate = (models) => {
        Payments.belongsTo(models.Merchants, {
            foreignKey: "merchantid",
            onDelete: 'CASCADE',
            as: "merchantOwner"

        });
        Payments.belongsTo(models.Customers, {
            foreignKey: "customerid",
            as: "customerOwner"
        })
    };*/
    return Payments;

};
