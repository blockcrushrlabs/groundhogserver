const AWS = require('aws-sdk');
const credentials = new AWS.SharedIniFileCredentials({profile: 'default'});
AWS.config.credentials = credentials;
AWS.config.update({region: 'us-east-1'});
const sqs = new AWS.SQS({apiVersion: "2012-11-05"});

module.exports = {
    createQ(req, res){

        if(!req.body.qName){
            return res.status(400).json({
                message: "Missing required information to create queue"
            })
        }
        let qName = req.body.qName;

        let params = {
            QueueName: qName,
            Attributes: {
                'DelaySeconds': '60',
                'MessageRetentionPeriod': '86400'
            }
        };

        sqs.createQueue(params, function(err, data){
            if(err){
                return res.status(400).json({
                    message: "there was an error",
                    error: err
                })
            }
            else{
                return res.status(200).json({
                    message: "Queue created",
                    data: data.QueueUrl
                })
            }
        })

    },
    getQURL(req, res){
        if(!req.body.qName){
            return res.status(400).json({
                message: "Missing required information to delete queue"
            })
        }
        let qName = req.body.qName;
        let params = {
            QueueName: qName
        };
        sqs.getQueueUrl(params, function(err, data){
            if(err){
                return res.status(400).json({
                    message: "There was an error retrieving queue name",
                    error: err
                })
            }
            else{
                return res.status(200).json({
                    message: "Queue name retrieved",
                    data: data.QueueUrl
                })
            }
        })
    },
    deleteQ(req, res){
        if(!req.body.qURL){
            return res.status(400).json({
                message: "Missing required information to delete queue"
            })
        }
        let qURL = req.body.qURL;

        let params = {
            QueueUrl: qURL
        };

        sqs.deleteQueue(params, function(err, data){
            if(err){
                return res.status(400).json({
                    message: "Error deleting Queue",
                    error: err
                })
            }
            else{
                return res.status(200).json({
                    message: "Queue ",
                    data: data
                })
            }
        })

    },
    postMessage(req, res){
        if(!req.body.message){
            return res.status(400).jsons({
                message: "Missing required field for message creation"
            })
        }
        if(!req.body.qURL) {
            return res.status(400).json({
                message: "Missing required field for message sending"
            })
        }
        let params = {
            MessageGroupId: "1",
            MessageBody: req.body.message,
            QueueUrl: req.body.qURL

        };
        sqs.sendMessage(params, function(err, data){
            if(err){
              return res.status(400).json({
                  message: "Error sending message",
                  error: err
              })
            }
            else{
                return res.status(200).json({
                    message: "Message sent succesfully"
                })
            }
        })

    }
};