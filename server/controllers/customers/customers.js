const Customer = require('../../models/index').Customers;
const EthCrypto = require('eth-crypto');
module.exports = {
    register(req, res) {
        let wallets;
        let confirms;
        if (!req.body.firstname || !req.body.lastname || !req.body.email) {
            return res.status(400).json({
                message: "Missing required fields"
            })
        }
        if (req.body.wallets) {
            wallets = req.body.wallets;
        }

        if(req.body.requiredconfirms){
            confirms = req.body.requiredconfirms;
        }
        else{
            confirms = -1;
        }
        console.log(confirms);
        return Customer.create({
            firstName: req.body.firstname,
            lastName: req.body.lastname,
            email: req.body.email,
            wallets: wallets,
            requiredConfirms: confirms,
            signatures: req.body.signatures
        }).then(function (customer) {
            return res.status(200).json({
                message: "Customer succesfully created",
                id: customer.id,
                apiKey: customer.apiKey
            })
        }).catch(function(err) {
            return res.status(400).json({
                message: "error saving customer",
                error: err
            })
        })
    },
    genSig(req, res){
        let message = req.body.message;
        let messageHash = EthCrypto.hash.keccak256(message);
        let signature = EthCrypto.sign(req.body.privateKey, messageHash);

        return res.status(200).json({
            message: signature
        })
    },
    update(req, res){
        let firstname;
        let lastname;
        let confirms;
        let wallets;
        Customer.findOne({
            where: {
                email: req.body.email
            }
        }).then(function(customer){
            if(!customer){
                return res.status(400).json({
                    message: "User with that email not found"
                })
            }
            if(customer.apiKey !== req.headers.authorization.split(" ")[1]){
                return res.status(400).json({
                    message: "Not authorized to update customer"
                })
            }
            if(req.body.firstname){
                firstname = req.body.firstname;
            }
            else{
                firstname = customer.firstName;
            }
            if(req.body.lastname){
                lastname = req.body.lastname;
            }
            else{
                lastname = customer.lastName;
            }
            if(req.body.confirms){
                confirms = req.body.confirms;
            }
            else{
                confirms = customer.requiredConfirms;
            }
            if(req.body.wallets){
                wallets = req.body.wallets;
            }
            else{
                wallets = customer.wallets;
            }
            customer.updateAttributes({
                firstName: firstname,
                lastName: lastname,
                requiredConfirms: confirms,
                wallets: wallets
            }).then(function(){
                return res.status(200).json({
                    message: "Customer updated"
                })
            }).catch(function(err){
                return res.status(400).json({
                    message: "there was an error saving customer info",
                    error: err
                })
            })
        }).catch(function(err){
            return res.status(400).json({
                message: "there was an error in customer lookup",
                error: err
            })
        })

    },
    delete(req, res){
        let confirm;

        Customer.findOne({
            where:{
                id: req.body.id
            }
        }).then(function(Customer){
            if(!Customer){
                return res.status(400).json({
                    message: "Customer with that Id not found"
                })
            }
            confirm = req.body.confirm;

            if(confirm !== "CONFIRM"){
                Customer.destroy({force: true});
                return res.status(200).json({
                    message: "CUSTOMER DELETED"
                })
            }
            else{
                return res.status(400).json({
                    message: "One of the provided parameters is incorrect"
                })
            }
        })
    }

};