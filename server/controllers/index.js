const merchants = require("./merchants/merchants");
const customers = require('./customers/customers');
const payments = require('./payments/payments');
const SQS = require('./sqsTest/SQS');

module.exports = {
    merchants,
    customers,
    payments,
    SQS
};
