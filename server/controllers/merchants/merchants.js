const Merchant = require('../../models/index').Merchants;

//issue api keys somewhere as part of this process
module.exports = {
    register(req, res){
        let isEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            .test(req.body.email);
        let isEthAddress = /^0x[a-fA-F0-9]{40}$/.test(req.body.ethAddress);

        if(!req.body.name){
            return res.status(400).json({
                message: "Name is required field for merchant sign up"
            })
        }
        if(!req.body.email){
            return res.status(400).json({
                message: "Email is a required field for merchant sign up"
            })
        }
        if(!isEmail){
            return res.status(400).json({
                message: "Email is not in appropriate format"
            })
        }
        if(req.body.ethAddress !== undefined) {
            if (!isEthAddress) {
                return res.status(400).json({
                    message: "Provided eth address is not valid"
                })
            }
        }
        return Merchant.create({
          name: req.body.name,
          ethAddress: req.body.ethaddress,
          imageRef: req.body.imageref,
          description: req.body.description,
          email: req.body.email,
        }).then(function(merchant){
            return res.status(200).json({
                message: "Merchant Created",
                apikey: merchant.apiKey
            })
        }).catch(function(err){
            if(err.name === "SequelizeUniqueConstraintError"){
                return res.status(400).json({
                    message: "Merchant name must be unique and email must be unique",
                })
            }

            return res.status(400).json({
                message: "error saving merchant",
                error: err
            })
        })
    },

    update(req, res){
        let newEthAddress;
        let newImageRef;
        let newDescription;
        let isEthAddress = /^0x[a-fA-F0-9]{40}$/.test(req.body.ethAddress);

        Merchant.findOne({
            where: {
                name: req.body.name
            }
        }).then(function(Merchant) {
            if(!Merchant){
                return res.status(400).json({
                    message: "no user found"
                })
            }

            if(!req.body.ethAddress && !req.body.imageref && !req.body.description){
                return res.status(400).json({
                    message: "nothing submitted to update"
                })
            }
            if(req.body.ethAddress){
                if(isEthAddress) {
                    newEthAddress = req.body.ethAddress;
                }
                else{
                    return res.status(400).json({
                        message: "ethaddress is not of correct format"
                    })
                }
            }
            else{
                newEthAddress = Merchant.ethAddress;
            }
            if(req.body.imageref){
                newImageRef = req.body.imageref;
            }
            else{
                newImageRef = Merchant.imageRef;
            }
            if(req.body.description){
                newDescription = req.body.description;
            }
            else{
                newDescription = Merchant.description;
            }
            Merchant.updateAttributes({
                ethAddress: newEthAddress,
                imageRef: newImageRef,
                description: newDescription,
            }).then(function(){
                return res.status(200).json({
                    message: "Merchant Updated"
                })
            }).catch(function(err){
                return res.status(400).json({
                    message: "error updating merchant",
                    error: err
                })
            })
        });
    },
    delete(req, res){
        let confirm;

        Merchant.findOne({
            where: {
                name: req.body.name
            }
        }).then(function(Merchant){
            if(!Merchant){
                return res.status(400).json({
                    message: "Merchant with that name not found"
                })
            }
            confirm = req.body.confirm;

            if (confirm !== "CONFIRM"){
                return res.status(400).json({
                    message: "Please appropriately confirm desire to delete"
                })
            }
            if(confirm === "CONFIRM"){
                Merchant.destroy({force: true});
                return res.status(200).json({
                    message: "MERCHANT DELETED"
                })
            }
            else{
                return res.status(400).json({
                    message: "One of the provided parameters is missing"
                })
            }
        })
    }
};

