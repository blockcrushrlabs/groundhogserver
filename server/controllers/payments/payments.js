const Payment = require('../../models/index').Payments;
const Merchant = require('../../models/index').Merchants;
const Customer = require('../../models/index').Customers;
const EthCrypto = require('eth-crypto');
const AWS = require('aws-sdk');
const fetch = require('node-fetch');
const credentials = new AWS.SharedIniFileCredentials({profile: 'default'});
AWS.config.credentials = credentials;
AWS.config.update({region: 'us-east-1'});
const sqs = new AWS.SQS({apiVersion: "2012-11-05"});

module.exports = {
    create(req, res){
        if(!req.body.merchname){
            return res.status(400).json({
                message: "missing required information"
            })
        }
        Merchant.findOne({
            where: {
                name: req.body.merchname
            }
        }).then(function(Merchant){
            if(!Merchant){
                return res.status(400).json({
                    message: "Merchant not found, please check provided merchant name"
                })
            }
            if(!req.body.transid || !req.body.value || !req.body.type || !req.body.frequency || !req.body.customeremail){
                return res.status(400).json({
                    message: "missing required information"
                })
            }
            Customer.findOne({
                where: {
                    email: req.body.customeremail
                }
            }).then(function(Customer) {
                if (!Customer) {
                    return res.status(400).json({
                        message: "No customer with that ID found"
                    })
                }
               /* if(Customer.requiredConfirms == -1){
                    return res.status(400).json({
                        message: "unable to make payments as this customer, customer must update required confirmations"
                    })
                }
                if (req.body.confirms) {
                    confirms = req.body.confirms;
                } */
                return Payment.create({
                    merchantId: Merchant.id,
                    merchantName: Merchant.name,
                    transId: req.body.transid,
                    value: req.body.value,
                    type: req.body.type,
                    frequency: req.body.frequency,
                    customerId: Customer.id
                    //confirms: confirms
                }).then(function () {
                    /*let params = {
                        MessageBody: ""
                    };*/

                    /*if(confirms < Customer.requiredconfirms){
                        return res.status(200).json({
                            message: "Payment data saved, payment will be posted when all required confirms are made"
                        })
                    }
                    else{*/
                        return res.status(200).json({
                            message: "payment data saved",
                            customer: Customer.id,
                            merchant: Merchant.id
                        })
                    //}
                }).catch(function (err) {
                    return res.status(400).json({
                        message: "error saving payment data",
                        error: err
                    })
                })
            })
        }).catch(function(err){
            return res.status(400).json({
                message: "Error saving transaction"
            })
        })

    },
    widgetPayment(req, res){
        if(!req.body.checkout || !req.body.confirmation){
            return res.status(400).json({
                message: "Error with provided payload"
            })
        }
        Customer.findOne({
            where: {
                email: req.body.checkout.email
            }
        }).then(function(foundCust){
            if (!foundCust) {
                //LOOK UP SAFE HERE AND IF NOT EXISTS CREATE IT HERE
                /*
                return Customer.create({
                    email: req.body.checkout.email,
                    firstName: req.body.checkout.firstName,
                    lastName: req.body.checkout.lastName,
                    signatures: req.body.ethAddress

                }).then(function(newCustomer){
                  */
                //handle new customer here
                /*
                }).catch(function(err){
                    return res.status(400).json({
                        message: "Error saving new customer",
                        error: err
                    })
                })*/
                return res.status(400).json({
                    message: "Customer not found, when finished this will create a customer before continuing the process"
                })
            }
            Merchant.findOne({
                where: {
                    apiKey: req.headers.authorization.split(" ")[1]
                }
            }).then(function(foundMerch) {

                if(!foundMerch){
                    return res.status(400).json({
                        message: "merchant not found, please ensure merchant exists before attempting to create a new subscription"
                    })
                }
                //deal with safestack here instead of database
                /*if (!req.body.confirmation.sigs[0].result) {
                    //wait for confirmations then send to
                    return res.status(400).json({
                        message: "missing confirmation signature"
                    })
                }
                */
                //let receivedMessage = req.body.confirmation.sigs[0].msg;

                //let testSig = EthCrypto.recover(req.body.confirmation.sigs[0].result, EthCrypto.hash.keccak256(receivedMessage));
                //if (testSig === foundCust.signatures) {
                    /*return res.status(200).json({
                        message: "SIGNATURE VERIFIED, generate token and send to merchant"
                    })
                    */
                    return Payment.create({
                        merchantId: foundMerch.id,
                        customerId: foundCust.id,
                        merchantName: foundMerch.name,
                        value: req.body.checkout.amount,
                        imageRef: foundMerch.imageRef
                    }).then(function(createdPay){
                        let params = {
                            MessageGroupId: "1",
                            MessageBody: JSON.stringify(req.body.checkout),
                            QueueUrl: req.body.qURL
                        };
                        //send sqs message to queue
                        /*sqs.sendMessage(params, function(err, data){
                            if(err){
                                return res.status(400).json({
                                    message: "There was an error posting message to queue",
                                    error: err
                                })
                            }
                                return res.status(200).json({
                                    message: "message posted wtih the following info",
                                    date: req.body.checkout,
                                    token: createdPay.uuid
                                })
                        });*/
                        return res.status(200).json({
                            data: req.body.checkout,
                            token: createdPay.uuid
                        })
                    }).catch(function(err){
                        return res.status(400).json({
                            message: "error saving payment",
                            error: err
                        })
                    })
                //}
                /*else {
                    return res.status(400).json({
                        testSig: testSig,
                        otherSig: foundCust.signatures
                    })
                }*/
            }).catch(function(err){
                return res.status(400).json({
                    error: err
                })
            })
        }).catch(function(err){
            return res.status(400).json({
                message: "error retrieving customer information",
                error: err
            })
        })
    },
    customerReport(req, res){
        Customer.findOne({
            where: {
                apiKey: req.headers.authorization.split(" ")[1]
            }
        }).then(function(Customer){
            if(!Customer){
                return res.status(400).json({
                    message: "Customer not found"
                })
            }
            Payment.findAll({
                where: {
                    customerId: Customer.id
                },
                attributes: ['merchantName', 'value', 'frequency', 'imageRef']
            }).then(function(report){
                if(!report){
                    return res.status(400).json({
                        message: "no subscriptions"
                    })
                }
                return res.status(200).json({
                    data: report
                })
            }).catch(function(err){
                return res.status(400).json({
                    message: "error in payment lookup",
                    error: err
                })
            });
        }).catch(function (err){
            return res.status(400).json({
                message: "Error retrieving customer info",
                error: err
            })
        })
    },
    processPaymentToken(req, res){
        if(!req.body.paymentToken){
            return res.status(400).json({
                message: "missing required information, please check provided parameters"
            })
        }
        Merchant.findOne({
            where: {
                apiKey: req.headers.authorization.split(" ")[1]
            }
        }).then(function(foundMerch){
            if(!foundMerch){
                return res.status(400).json({
                    message: "merchant not found"
                })
            }
            Payment.findOne({
                where: {
                    uuid: req.body.paymentToken
                }
            }).then(function(foundPayment){
                if(!foundPayment){
                    return res.status(400).json({
                        message: "no matching payment found, please recheck provided token"
                    })
                }
                if(foundPayment.merchantId !== foundMerch.id){
                    return res.status(400).json({
                        message: "you are not authorized to process this payment"
                    })
                }

                return res.status(200).json({
                    message: "posted to sqs queue"
                })

            }).catch(function(err){
                return res.status(400).json({
                    message: "error retrieving payment info",
                    error: err
                })
            })

        }).catch(function(err){
            return res.status(400).json({
                message: "Error retrieving merchant info",
                error: err
            })
        })
    },
    merchantReport(req, res){
        Merchant.findOne({
            where: {
                    apiKey: req.headers.authorization.split(" ")[1]
            }
        }).then(function(foundMerch){
            let paymentTotal;
            Payment.count({
                where: {
                    merchantId: foundMerch.id
                }
            }).then(function(total){
                paymentTotal = total
            }).catch(function(err){
                return res.status(400).json({
                    message: "error counting payments",
                    error: err
                })
            });
            let orderSort = "";
            if(req.query.sortOrder == 'descend'){
                orderSort = "DESC"
            }
            else{
                orderSort = "ASC"
            }
            let offset = req.query.results * (req.query.page - 1);

            Payment.findAll({
                limit: req.query.results,
                offset: offset,
                order: [[req.query.sortField, orderSort]],
                where: {
                    merchantId: foundMerch.id
                },
                attributes: ['transId', 'value', 'type', 'frequency']
            }).then(function(report){
                if(!report){
                    return res.status(400).json({
                        message: "no payments to display"
                    })
                }
                return res.status(200).json({
                    report: report
                })

            }).catch(function(err){
                return res.status(400).json({
                    message: "error generating merchant report "
                })
            })
        }).catch(function(err){
            return res.status(400).json({
                message: "error retrieving merchant",
                error: err
            })
        })
    },
    cancelSub(req, res){
        Customer.findOne({
            where: {
                apiKey: req.headers.authorization.split(" ")[1]
            }
        }).then(function(Customer){
            if(!Customer){
                return res.status(400).json({
                    message: "Customer not found"
                })
            }

        }).catch(function (err){
            return res.status(400).json({
                message: "Error retrieving customer info",
                error: err
            })
        })

    }
};